package azagwen.mob_waves.server;

import azagwen.mob_waves.server.event.MobSpawner;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import net.minecraft.command.argument.EntitySummonArgumentType;
import net.minecraft.command.suggestion.SuggestionProviders;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public record CommandRegistry(MobSpawner spawner) {

    public void registerCommands(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(this.createCommand("setSpawnActive", 2).then(this.booleanArgument("value").executes(cmd -> {
            return this.setSpawnActive(cmd.getSource(), BoolArgumentType.getBool(cmd, "value"));
        })));
        dispatcher.register(this.createCommand("setSpawnMob", 2).then(this.entitySummonArgument("entity").suggests(SuggestionProviders.SUMMONABLE_ENTITIES).executes(cmd -> {
            return this.setSpawnMob(cmd.getSource(), EntitySummonArgumentType.getEntitySummon(cmd, "entity"));
        })));
        dispatcher.register(this.createCommand("setSpawnInterval", 2).then(this.intArgument("minSecs", 1).then(this.intArgument("maxSecs", 1).executes(cmd -> {
            return this.setSpawnInterval(cmd.getSource(), IntegerArgumentType.getInteger(cmd, "minSecs"), IntegerArgumentType.getInteger(cmd, "maxSecs"));
        }))));
        dispatcher.register(this.createCommand("setSpawnCount", 2).then(this.intArgument("min", 1).then(this.intArgument("max", 1).executes(cmd -> {
            return this.setSpawnCount(cmd.getSource(), IntegerArgumentType.getInteger(cmd, "min"), IntegerArgumentType.getInteger(cmd, "max"));
        }))));
        dispatcher.register(this.createCommand("setSpawnRadius", 2).then(this.intArgument("radius", 1).executes(cmd -> {
            return this.setSpawnRadius(cmd.getSource(), IntegerArgumentType.getInteger(cmd, "radius"));
        })));
    }

    private ArgumentBuilder<ServerCommandSource, LiteralArgumentBuilder<ServerCommandSource>> createCommand(String name, int permissionLevel) {
        return CommandManager.literal(name).requires(cmd -> cmd.hasPermissionLevel(permissionLevel));
    }

    private RequiredArgumentBuilder<ServerCommandSource, Integer> intArgument(String name, int min) {
        return CommandManager.argument(name, IntegerArgumentType.integer(min));
    }

    private RequiredArgumentBuilder<ServerCommandSource, Integer> intArgument(String name, int min, int max) {
        return CommandManager.argument(name, IntegerArgumentType.integer(min, max));
    }

    private RequiredArgumentBuilder<ServerCommandSource, Boolean> booleanArgument(String name) {
        return CommandManager.argument(name, BoolArgumentType.bool());
    }

    private RequiredArgumentBuilder<ServerCommandSource, Identifier> entitySummonArgument(String name) {
        return CommandManager.argument(name, EntitySummonArgumentType.entitySummon());
    }

    private int setSpawnActive(ServerCommandSource cmd, boolean active) {
        this.spawner.setSpawningActive(active);
        if (active) {
            cmd.sendFeedback(new TranslatableText(this.getCommandTranslationKey("set_spawn_active.enabled")), true);
        } else {
            cmd.sendFeedback(new TranslatableText(this.getCommandTranslationKey("set_spawn_active.disabled")), true);
        }
        return 1;
    }

    private int setSpawnMob(ServerCommandSource cmd, Identifier entityType) {
        this.spawner.setMobToSpawn(entityType);
        cmd.sendFeedback(new TranslatableText(this.getCommandTranslationKey("set_spawn_mob"), Registry.ENTITY_TYPE.get(entityType).getName()), true);
        return 1;
    }

    private int setSpawnInterval(ServerCommandSource cmd, int minSecs, int maxSecs) {
        this.spawner.setInterval(minSecs, maxSecs);
        cmd.sendFeedback(new TranslatableText(this.getCommandTranslationKey("set_spawn_interval"), minSecs, maxSecs), true);
        return 1;
    }

    private int setSpawnCount(ServerCommandSource cmd, int min, int max) {
        this.spawner.setNumMobsToSpawn(min, max);
        cmd.sendFeedback(new TranslatableText(this.getCommandTranslationKey("set_spawn_count"), min, max), true);
        return 1;
    }

    private int setSpawnRadius(ServerCommandSource cmd, int dist) {
        this.spawner.setMaxSpawnDist(dist);
        cmd.sendFeedback(new TranslatableText(this.getCommandTranslationKey("set_spawn_radius"), dist), true);
        return 1;
    }

    private String getCommandTranslationKey(String commandName) {
        return "command.mob_waves." + commandName + ".feedback";
    }
}