package azagwen.mob_waves.server.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.player.PlayerEntity;

public interface PlayerTickCallBack {
    Event<PlayerTickCallBack> EVENT = EventFactory.createArrayBacked(PlayerTickCallBack.class, (listeners) -> (player) -> {
        for (var listener : listeners) {
            listener.tick(player);
        }
    });

    void tick(PlayerEntity player);
}
