package azagwen.mob_waves;

import azagwen.mob_waves.server.CommandRegistry;
import azagwen.mob_waves.server.event.MobSpawner;
import azagwen.mob_waves.server.event.PlayerTickCallBack;
import azagwen.mob_waves.server.event.ServerTickCallBack;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MobWaves {
    public static final Logger LOGGER = LogManager.getLogger("Mob Waves");

    // Runs the mod initializer on the client environment.
    @Environment(EnvType.CLIENT)
    public static void initClient() {
    }

    //Runs the mod initializer.
    public static void initMain() {
        var spawner = new MobSpawner();

        PlayerTickCallBack.EVENT.register(spawner::onPlayerTick);
        ServerTickCallBack.EVENT.register(spawner::onServerTick);
        CommandRegistrationCallback.EVENT.register((dispatcher, dedicated) -> {
            var commands = new CommandRegistry(spawner);
            commands.registerCommands(dispatcher);
        });
    }
}
