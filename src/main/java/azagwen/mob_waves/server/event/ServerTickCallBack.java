package azagwen.mob_waves.server.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;

import java.util.function.BooleanSupplier;

public interface ServerTickCallBack {
    Event<ServerTickCallBack> EVENT = EventFactory.createArrayBacked(ServerTickCallBack.class, (listeners) -> (shouldKeepTicking) -> {
        for (var listener : listeners) {
            listener.tick(shouldKeepTicking);
        }
    });

    void tick(BooleanSupplier shouldKeepTicking);
}
