# Mob Waves

A commands driven Mob Waves mod.

# Commands

Every command in the mod and their arguments.

- setSpawnActive
  - boolean `(default: false)`
- setSpawnMob
  - mod identifier `(default: minecraft:creeper)`
- setSpawnInterval 
  - min seconds `(default: 45s)`
  - max seconds `(default: 120s)`
- setSpawnCount
  - min count `(default: 40)`
  - max count `(default: 10)`
- setSpawnRadius
  - max distance `(default: 30)`
