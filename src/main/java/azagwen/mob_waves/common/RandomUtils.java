package azagwen.mob_waves.common;

import java.util.Random;

public final class RandomUtils
{
    public static float randomSign(float i) {
        return i * ((Math.random() < 0.5D) ? -1 : 1);
    }

    public static float randomSign(float i, Random random) {
        return i * ((random.nextDouble() < 0.5D) ? -1 : 1);
    }

    public static double randBetween(double min, double max) {
        return min + Math.random() * (max - min);
    }

    public static double randBetween(double min, double max, Random random) {
        return min + random.nextDouble() * (max - min);
    }

    public static int randBetween(int min, int max) {
        return min + (int)(Math.random() * (max - min + 1));
    }

    public static int randBetween(int min, int max, Random random) {
        return min + (int)(random.nextDouble() * (max - min + 1));
    }
}