package azagwen.mob_waves.server.event;

import azagwen.mob_waves.common.RandomUtils;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BooleanSupplier;

import static azagwen.mob_waves.MobWaves.LOGGER;

public class MobSpawner {
    private static final int TICKS_PER_SECOND = 20;
    private static final int SPAWN_HEIGHT = 2;
    private final Set<Integer> playersDoneSpawning = new HashSet<>();
    private boolean spawnReady;
    private boolean active = true;
    private Identifier mobToSpawn = new Identifier("minecraft", "creeper");
    private int minMobsToSpawn = 10;
    private int maxMobsToSpawn = 40;
    private int minIntervalTicks = 900;
    private int maxIntervalTicks = 2400;
    private int maxSpawnDist = 30;
    private int ticksUntilNextWave = this.minIntervalTicks;

    private static class NoGroundException
      extends Exception
    {
      private static final long serialVersionUID = 1L;
    }

    public void onServerTick(BooleanSupplier shouldKeepTicking) {
        if (!this.active) {
            return;
        }

        this.ticksUntilNextWave--;

        if (this.ticksUntilNextWave == 0) {
            this.spawnReady = true;
            this.playersDoneSpawning.clear();
            this.scheduleNextWave();
        }
    }

    public void onPlayerTick(PlayerEntity player) {
        if (player.getWorld().isClient()) {
            return;
        }

        var playerId = player.getId();

        if (this.spawnReady && !this.playersDoneSpawning.contains(playerId)) {
              this.spawnMobs(player);
              this.playersDoneSpawning.add(playerId);
        }
    }

    private void spawnMobs(PlayerEntity player) {
        var numMobs = RandomUtils.randBetween(this.minMobsToSpawn, this.maxMobsToSpawn);
        for (int i = 0; i < numMobs; i++) {
            this.spawnMob(player);
        }
    }

    private void spawnMob(PlayerEntity player) {
        var y = 0.0D;
        var level = (ServerWorld) player.getWorld();
        var x = player.getX() + RandomUtils.randBetween(-this.maxSpawnDist, this.maxSpawnDist);
        var z = player.getZ() + RandomUtils.randBetween(-this.maxSpawnDist, this.maxSpawnDist);

        try {
            y = (this.getGroundLevel(level, x, z) + 2);
        } catch (NoGroundException ex) {
            return;
        }

        var pos = new BlockPos(x, y, z);

        if (!World.isValid(pos)) {
            return;
        }

        var compoundTag = new NbtCompound();
        compoundTag.putString("id", this.mobToSpawn.toString());
        var entity = EntityType.loadEntityWithPassengers(compoundTag, level, e -> {
              e.updatePositionAndAngles(pos.getX(), pos.getY(), pos.getZ(), e.getYaw(), e.getPitch());
              return e;
        });

        if (entity instanceof MobEntity mobEntity) {
            mobEntity.initialize(level, level.getLocalDifficulty(entity.getBlockPos()), SpawnReason.COMMAND, null, null);
        } else {
            LOGGER.warn("Failed to load mob: " + this.mobToSpawn);
            return;
        }

        if (!level.spawnEntity(entity)) {
            LOGGER.warn("Failed to spawn mob: " + entity);
        }
    }

    private int getGroundLevel(World world, double x, double z) throws NoGroundException {
        var height = world.getHeight();
        var maxY = height - 1;
        var inCeiling = false;

        for (int y = maxY; y > 0; y--) {
            var blockPos = new BlockPos(x, y, z);
            var foundSolid = this.isSolid(world, blockPos);

            if (foundSolid) {
              if (y == maxY) {
                inCeiling = true;
              }
              else if (!inCeiling) {
                return y;
              }
            } else {
              inCeiling = false;
            }
        }

        throw new NoGroundException();
    }

    private boolean isSolid(World world, BlockPos pos) {
        var blockState = world.getBlockState(pos);
        return (blockState.isSolidBlock(world, pos));
    }

    private void scheduleNextWave() {
        this.ticksUntilNextWave = RandomUtils.randBetween(this.minIntervalTicks, this.maxIntervalTicks);
    }

    public void setMobToSpawn(Identifier mobToSpawn) {
      this.mobToSpawn = mobToSpawn;
    }

    public void setInterval(int minSecs, int maxSecs) {
        this.minIntervalTicks = minSecs * 20;
        this.maxIntervalTicks = maxSecs * 20;
        this.scheduleNextWave();
    }

    public void setSpawningActive(boolean active) {
      this.active = active;
    }

    public void setNumMobsToSpawn(int minCount, int maxCount) {
        this.minMobsToSpawn = minCount;
        this.maxMobsToSpawn = maxCount;
    }

    public void setMaxSpawnDist(int maxSpawnDist) {
      this.maxSpawnDist = maxSpawnDist;
    }
}